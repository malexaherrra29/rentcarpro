package com.example.rent_a_car_d.ui.gallery;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.rent_a_car_d.MainActivity;
import com.example.rent_a_car_d.R;
import com.example.rent_a_car_d.Splash;
import com.example.rent_a_car_d.admin;
import com.example.rent_a_car_d.nav_Login;
import com.example.rent_a_car_d.ui.slideshow.SlideshowFragment;

public class GalleryFragment extends Fragment {
      View VISTA;
      Button btnacc;
      Button btning;
      EditText txtusu, txtpass;
      ImageView im1;
      Button btnadm;

    private GalleryViewModel galleryViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {

        galleryViewModel = ViewModelProviders.of(this).get(GalleryViewModel.class);
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        final Button btnacc = (Button) view.findViewById(R.id.btnAcceder);
        btnacc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent acceder = new Intent(getActivity(), MainActivity.class);
                startActivity(acceder);
            }
        });

        btnadm = (Button) view.findViewById(R.id.btnAdm);
        im1=(ImageView) view.findViewById(R.id.img1);



        txtusu =(EditText) view.findViewById(R.id.txtusuario);
        txtpass =(EditText) view.findViewById(R.id.txtpass);

        String usuario =txtusu.getText().toString();
        String passwor =txtpass.getText().toString();



        Button btning = (Button) view.findViewById(R.id.btn_ingresar);
        btning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String usuario =txtusu.getText().toString();
                String passwor =txtpass.getText().toString();
                if (TextUtils.isEmpty(usuario) && TextUtils.isEmpty(passwor)){
                    txtusu.setError(getString(R.string.txt_usuario));
                    txtusu.requestFocus();
                    txtpass.setError(getString(R.string.txt_conraseña));
                    txtpass.requestFocus();
                }else {
                    Intent ingresar = new Intent(getActivity(), Splash.class);
                    startActivity(ingresar);
                }


            }
        });

        final Button btnadm = (Button) view.findViewById(R.id.btnAdm);
        btnadm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent adm = new Intent(getActivity(), admin.class);
                startActivity(adm);
            }
        });


        return view;







    }






}