package com.example.rent_a_car_d;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
        Button btnaceptar;
        EditText txtnom, txtcedu, txtmail, txttelf, txtcont ;
        ImageView im1;

        FirebaseAuth mAuth;
        DatabaseReference mDatabase;


        private  String nombre;
        private String cedula;
        private  String email;
        private  String telefono;
        private  String contraseña;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtnom = (EditText)findViewById(R.id.txtnombre);
        txtcedu = (EditText)findViewById(R.id.txtcedula);
        txtmail = (EditText)findViewById(R.id.txtemail);
        txttelf = (EditText)findViewById(R.id.txttelefono);
        txtcont = (EditText)findViewById(R.id.txtcontraseña);
        btnaceptar =(Button)findViewById(R.id.btnaceptar);




        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

    }

   /* public boolean validar(String mail){

        Pattern este = Patterns.EMAIL_ADDRESS;
        return este.matcher(mail).matches();

        if (!validar("miemail@gmail.com")){
            txtmail.setError("email no valido ");
        }
    }*/


    public void registrase(View view){

         nombre = txtnom.getText().toString();
         cedula = txtcedu.getText().toString();
         email = txtmail.getText().toString();
         telefono = txttelf.getText().toString();
         contraseña = txtcont.getText().toString();


        //int telefono2 =Integer.parseInt(telefono);



        if ( email.isEmpty() && cedula.isEmpty() && contraseña.length()<8 ) {
            txtnom.setError(getString(R.string.txt_nobre));
            txtnom.requestFocus();
            txtcedu.setError(getString(R.string.txt_cedula));
            txtcedu.requestFocus();
            txtmail.setError(getString(R.string.txt_email));
            txtmail.requestFocus();
            txttelf.setError(getString(R.string.txt_telefono));
            txttelf.requestFocus();
            txtcont.setError(getString(R.string.txt_conraseña_8));
            txtcont.requestFocus();

            }else{
                registra();

            }
        }






    private void registra() {

       mAuth.createUserWithEmailAndPassword(email,contraseña).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
           @Override
           public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){

                    Map<String, Object> map = new HashMap<>();
                    map.put("name", nombre);
                    map.put("cedula", cedula);
                    map.put("correo", email);
                    map.put("telefono", telefono);
                    map.put("contraseña", contraseña);

                    String id = mAuth.getCurrentUser().getUid();
                    mDatabase.child("Users").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task2) {
                            if (task2.isSuccessful()){
                                startActivity(new Intent(MainActivity.this, menuActivity.class));
                                Toast.makeText(MainActivity.this,"Registro con exito",Toast.LENGTH_LONG).show();
                            }else {
                                Toast.makeText(MainActivity.this,"no se crearon los datos correctamente",Toast.LENGTH_LONG).show();
                            }

                        }
                    });

                }else {
                    Toast.makeText(MainActivity.this, "no s epudo almacenar usuario", Toast.LENGTH_LONG).show();
                }
           }
       });

        Intent enviar = new Intent(getApplicationContext(), Main2Activity.class);
        startActivity(enviar);

    }



    public void imagen(View view) {
       cargarImagen();
    }

    private void cargarImagen() {
        im1 =(ImageView)findViewById(R.id.img1);
        Intent imagen = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        imagen.setType("image/");
        startActivityForResult(imagen.createChooser(imagen,"seleccione la aplicacion"),10);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            Uri path= data.getData();
            im1.setImageURI(path);

        }
    }

}
