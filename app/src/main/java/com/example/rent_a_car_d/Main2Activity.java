package com.example.rent_a_car_d;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class Main2Activity extends AppCompatActivity {
    View VISTA;
    Button btnacc, btnadm;
    Button btning;
    EditText txtusu, txtpass;
    ImageView im1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Button btnacc = (Button) findViewById(R.id.btnAcceder);
        btnacc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent acceder = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(acceder);
            }
        });

        btnadm = (Button)findViewById(R.id.btnAdm2);

        btnadm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent admi= new Intent(getApplicationContext(), admin.class);
                startActivity(admi);
            }
        });




        im1 = (ImageView) findViewById(R.id.img1);


        txtusu = (EditText) findViewById(R.id.txtusuario);
        txtpass = (EditText) findViewById(R.id.txtpass);

        String usuario = txtusu.getText().toString();
        String passwor = txtpass.getText().toString();


        Button btning = (Button) findViewById(R.id.btn_ingresar);
        btning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String usuario = txtusu.getText().toString();
                String passwor = txtpass.getText().toString();
                if (TextUtils.isEmpty(usuario) && TextUtils.isEmpty(passwor)) {
                    txtusu.setError(getString(R.string.txt_usuario));
                    txtusu.requestFocus();
                    txtpass.setError(getString(R.string.txt_conraseña));
                    txtpass.requestFocus();
                } else {
                    Intent ingresar = new Intent(getApplicationContext(), menuActivity.class);
                    startActivity(ingresar);
                }


            }
        });





    }
}